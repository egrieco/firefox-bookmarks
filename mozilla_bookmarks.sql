select b.title,p.url
from moz_bookmarks as b
join moz_places as p on b.fk = p.id
where b.type = 1
order by b.lastModified desc;
