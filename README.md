# firefox-bookmarks

A project to enable easy dumping of bookmarks from [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/) so that they may be processed from the command line with tools like [fzf](https://github.com/junegunn/fzf) (a command-line fuzzy finder).

## Files

Currently, this repository is just a bunch of separate files that can be used from the command line:

- [dump_bookmarks_csv.sqlite](dump_bookmarks_csv.sqlite) - the commands to be run in sqlite3
- [mozilla_bookmarks.sql](mozilla_bookmarks.sql) - the actual SQL query to generate the bookmarks

## Usage

To run on macOS:

```shell
sqlite3 ~/Library/Application\ Support/Firefox/Profiles/PROFILE_DIRECTORY/places.sqlite < dump_bookmarks.sqlite > OUTPUT_BOOKMARKS.csv
```

- `PROFILE_DIRECTORY` will be in the form of `HASH.NAME`
- `OUTPUT_BOOKMARKS` can be anything you want
